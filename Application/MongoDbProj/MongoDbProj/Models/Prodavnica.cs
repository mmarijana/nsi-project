﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace MongoDbProj.Models
{
    public class Prodavnica
    {
        public ObjectId Id { get; set; }
       public int IdProdavnice { get; set; }

        public string Ime { get; set; }
        public string Adresa { get; set; }
        public string Sifra { get; set; }
        public List<ObjectId> proizvodi { get; set; }

        public Prodavnica()
        {
            proizvodi = new List<ObjectId>();
        }
    }
}
