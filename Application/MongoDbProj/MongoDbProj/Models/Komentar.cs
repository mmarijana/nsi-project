﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace MongoDbProj.Models
{
    public class Komentar
    {
        public ObjectId Id { get; set; }

        public string TekstKomentara { get; set; }
        public Korisnik KorisnikKom { get; set; }
        public string Sifra { get; set; }  //je emaikorisnika
    }
}
