﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.Extensions.Configuration;
using System.IO;
using System.Text;
namespace MongoDbProj.AppConfig
{
    public static class AppConfiguration
    {
         
            private static IConfiguration currentConfig;

            public static void SetConfig(IConfiguration configuration)
            {
                currentConfig = configuration;
            }


            public static string GetConfiguration(string configKey)
            {
                try
                {
                    string connectionString = currentConfig.GetConnectionString(configKey);
                    return connectionString;
                }
                catch (Exception ex)
                {
                    throw (ex);
                }
                return "";
            }

        }
    }

