﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDbProj.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;

using MongoDbProj.AppConfig;
using System.Reflection.Metadata.Ecma335;
using Microsoft.AspNetCore.Mvc.Localization;

namespace MongoDbProj.Controllers
{
    public class AdminController : Controller
    {
        clsMongoDBDataContext _dbContextKorisnici = new clsMongoDBDataContext("korisnici");
        clsMongoDBDataContext _dbContextProdavnica = new clsMongoDBDataContext("prodavnice");
        clsMongoDBDataContext _dbContextKorpe = new clsMongoDBDataContext("korpe");
        clsMongoDBDataContext _dbContextProizvodi = new clsMongoDBDataContext("proizvodi");
        clsMongoDBDataContext _dbContextAdmini = new clsMongoDBDataContext("admini");


        public IActionResult osnovniPrikazAdminu()
        {
            
            return View();

        }
        public IActionResult idiNaRegistrujAdmin()
        {
            Admin k = new Admin();
            return View(k);

        }
     
         public IActionResult registrujAdmin(Admin p)
          {
              //mora da bude sve uneseno ,provera da nije nesto null

              if (p.Password == null || p.Email == null)
              {
                
                var a = "Popuni sva polja!";
                TempData["msgPopuni"]= JsonConvert.SerializeObject(a.ToString());
                return  RedirectToAction("idiNaRegistrujAdmin","Admin");

              }
              else
              {

                //ne postoji registrujemo ga
                  var nameFilter = Builders<Admin>.Filter.Eq("Password", p.Password) & Builders<Admin>.Filter.Eq("Email", p.Email);
                  var admin = this._dbContextAdmini.GetAdmini.Find(nameFilter).FirstOrDefault();

                  
                  if (admin == null)
                  {
                      p.Id = ObjectId.GenerateNewId();
                      
                      this._dbContextAdmini.GetAdmini.InsertOne(p);

                      //ide na straniuc gde ce videti prikaz za admin 
                     return  RedirectToAction("osnovniPrikazAdminu","Admin");


                  }
                  else
                  {
                    //vracamo se na strani reg forme i obavestavamo da posti nalog
                    //ili da promeni ili da se loguje
                    TempData["msgPostoji"] = JsonConvert.SerializeObject("Nalog vec postoji!");

                    return RedirectToAction("idiNaRegistrujAdmin","Admin");


                  }
                     

              }



          }
        public IActionResult idiNaLogAdmin()
        {
            Admin k = new Admin();
            return View(k);
        }


        //ovo se izvrsava kada korisnik ide na loguj admina
        public IActionResult logAdmin(Admin k)
        {

            var nameFilter = Builders<Admin>.Filter.Eq("Password", k.Password) & Builders<Admin>.Filter.Eq("Email", k.Email);
            var korisnik = this._dbContextAdmini.GetAdmini.Find(nameFilter).FirstOrDefault();

           
            //postoji kao admin
            if (korisnik != null)
            {
                //redirect na straancu gde vidi samo admin
                return RedirectToAction("osnovniPrikazAdminu", "Admin");

            }
            else //ili nije reg kao admin vec kao korisnik
                 //ili je reg kao admin ali je pogresio 
            
            {
                TempData["msgGreska"] = JsonConvert.SerializeObject("Pogresan password ili Email!");

                return RedirectToAction("idiNaLogAdmin","Admin");

            }


        }
       

    }

}
