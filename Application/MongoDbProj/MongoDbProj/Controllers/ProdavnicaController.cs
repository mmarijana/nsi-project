﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MongoDbProj.Models;

using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Reflection.Metadata.Ecma335;
using Microsoft.AspNetCore.Mvc.Localization;
using MongoDbProj.AppConfig;
namespace MongoDbProj.Controllers
{
    public class ProdavnicaController : Controller
    {
        clsMongoDBDataContext _dbContext = new clsMongoDBDataContext("prodavnice");
    
        public async Task<ActionResult> prikaziProdavnice()
        {

            
            IEnumerable<Prodavnica> products = null;
            using (IAsyncCursor<Prodavnica> cursor = await this._dbContext.GetProdavnice.FindAsync(new BsonDocument()))
            {
                while (await cursor.MoveNextAsync())
                {
                    products = cursor.Current;
                }
            }
            return View(products);

        }

        /*
        public IActionResult obrisiProdavnicu(string Id)
        {
            var connectionString = "mongodb://localhost/?safe=true";

            var client = new MongoClient(connectionString);
            var database = client.GetDatabase("Ishop");
            var prodavniceCollection = database.GetCollection<Prodavnica>("prodavnice");
            var proizvodCollection = database.GetCollection<Proizvod>("proizvodi");

            //id prodavnice
            var objId = ObjectId.Parse(Id);
            var filter = Builders<Prodavnica>.Filter.Eq("_id", objId);
            var prodav = prodavniceCollection.Find(filter).FirstOrDefault();

            ///brisanje proizvoda prodavnice
            foreach (ObjectId idd in prodav.proizvodi)
            {
                //brise refernecu iz niza
               // prodav.proizvodi.Remove(idd);
                //birse proizvod iz baze
                var filter1 = Builders<Proizvod>.Filter.Eq("_id", idd);
                var r = proizvodCollection.DeleteOne(filter1);

               /// var update = Builders<Prodavnica>.Update.Set("proizvodi", prodav.proizvodi);
                //prodavniceCollection.UpdateOne(filter, update);
            }
            for(int i=0; i<prodav.proizvodi.Count(); i++)
            {
                //brise refernecu iz niza
                prodav.proizvodi.RemoveAt(i);
                
            }
            var update = Builders<Prodavnica>.Update.Set("proizvodi", prodav.proizvodi);
            prodavniceCollection.UpdateOne(filter, update);


            var rez =prodavniceCollection.DeleteMany(filter);
            
         

            return RedirectToAction("prikaziProdavnice");

        }
           */
        
       
       
        
    }
}